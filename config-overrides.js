
const {
  paths,
} = require('react-app-rewired');

const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin,
} = require('customize-cra');


module.exports = {
  paths: (paths) => {
    paths.appHtml = `${paths.appSrc}/index.html`;
    return paths;
  },
  webpack: (config, env) => {
    return override(
      addDecoratorsLegacy(),
      removeModuleScopePlugin(),
      addWebpackAlias({
        '@': paths.appSrc,
        '@mes/mes-shared': `${paths.appSrc}/mesShared`,
        '@mes/mes-ui-react': `${paths.appSrc}/mesUiReact`,
      }),
    )(config, env);
  },
  devServer: (configFunction) => (proxy, allowedHost) => {
    //
    const { proxy : propertiesProxy } = require('./config/devServer.properties.js');
    let targetProxy = proxy;

    if (!targetProxy && propertiesProxy && typeof propertiesProxy === 'object') {
      targetProxy = Object.entries(propertiesProxy).reduce((prev, [contextKey, context]) => ({
        ...prev,
        [contextKey]: {
          ...context,
          changeOrigin: 'changeOrigin' in context ? context.changeOrigin : true,
          secure: 'secure' in context ? context.secure : false,
        },
      }), {});
    }

    return configFunction(targetProxy, allowedHost);
  },
};
