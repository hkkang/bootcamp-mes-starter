

const devServerProperties = {
  proxy: {
    // mes-starer-service를 MicroServiceName으로 수정
    '/mes-starter-service': {
      target: 'http://localhost:3010',             // to json-server
    },
  },
};

module.exports = devServerProperties;
