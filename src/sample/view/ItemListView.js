
import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { mesAutobind } from '@mes/mes-shared';
import { Table, Icon } from 'semantic-ui-react';


@mesAutobind
@observer
class ItemListView extends Component {
  //
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
  };

  render() {
    //
    const {
      items,
    } = this.props;

    return (
      <Table celled>
        <Table.Header>
          <Table.Row textAlign="center">
            <Table.HeaderCell>재료번호</Table.HeaderCell>
            <Table.HeaderCell>상위 번호</Table.HeaderCell>
            <Table.HeaderCell>형태</Table.HeaderCell>
            <Table.HeaderCell>스테이지</Table.HeaderCell>
            <Table.HeaderCell>가공일시</Table.HeaderCell>
            <Table.HeaderCell>상세보기</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {
            Array.isArray(items) && items.length > 0 ?
              items.map((item) =>
                <Table.Row key={item.itemNo}>
                  <Table.Cell>{item.itemNo}</Table.Cell>
                  <Table.Cell>{item.parentNo}</Table.Cell>
                  <Table.Cell>{item.shapeType}</Table.Cell>
                  <Table.Cell>{item.stage}</Table.Cell>
                  <Table.Cell>{item.cutDateTime}</Table.Cell>
                  <Table.Cell textAlign="center">
                    <Link to={`/items/${item.itemNo}`}><Icon name="angle right" /></Link>
                  </Table.Cell>
                </Table.Row>
              )
              :
              <Table.Row>
                <Table.Cell colSpan="6" textAlign="center">데이터가 없습니다.</Table.Cell>
              </Table.Row>
          }
        </Table.Body>
      </Table>
    );
  }
}

export default ItemListView;
