
import React, { Component, Fragment } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { mesAutobind } from '@mes/mes-shared';
import ItemAttrListView, { type } from './ItemAttrListView';


@mesAutobind
@observer
class ItemView extends Component {
  //
  static propTypes = {
    item: PropTypes.object,
  };

  render() {
    //
    const {
      item,
    } = this.props;

    if (!item) {
      return null;
    }

    return (
      <Fragment>
        <ItemAttrListView
          type={type.STATUS}
          list={item.status}
        />
        <ItemAttrListView
          type={type.LOCATION}
          list={item.location}
        />
        <ItemAttrListView
          type={type.SIZE_WEIGHT}
          list={item.sizeWeight}
        />
        <ItemAttrListView
          type={type.ORDER}
          list={item.order}
        />
        <ItemAttrListView
          type={type.YARD_LOCATION}
          list={item.yardLocation}
        />
        <ItemAttrListView
          type={type.ATTRS}
          list={item.attrs}
        />
      </Fragment>
    );
  }
}

export default ItemView;
