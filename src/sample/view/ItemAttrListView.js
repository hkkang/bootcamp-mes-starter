
import React, { Component, Fragment } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { mesAutobind } from '@mes/mes-shared';
// import { Header, Divider, Table } from '@mes/mes-ui-react';
import { Header, Divider, Table } from 'semantic-ui-react';


@mesAutobind
@observer
class ItemAttrListView extends Component {
  //
  static propTypes = {
    type: PropTypes.object,
    list: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    type: null,
    list: [],
  };

  static type = {
    STATUS: {
      name: '재료 상태',
      attrNames: ['shapeType', 'status', 'statusDateTime', 'reason'],
      headers: ['재료형태 코드', '재료상태', '재료상태 변경 일시', '재료상태 변경 이유'],
    },
    LOCATION: {
      name: '공장위치',
      attrNames: ['plant', 'factory', 'process', 'machineNo', 'changeDateTime'],
    },
    SIZE_WEIGHT: {
      name: '크기 & 무게',
      attrNames: ['width', 'thickness', 'length', 'weight', 'changeDateTime'],
    },
    ORDER: {
      name: '주문정보',
      attrNames: ['orderNo', 'changeOrderDateTime'],
    },
    YARD_LOCATION: {
      name: '야드 적재 위치',
      attrNames: ['greateTp', 'middleTp', 'smallTp', 'storeCd', 'changeDateTime'],
    },
    ATTRS: {
      name: '추가속성',
      attrNames: ['name', 'type', 'value'],
    },
  };


  render() {
    //
    const {
      type,
      list,
    } = this.props;

    return (
      <Fragment>
        <Header as="h2">
          {type.name}
        </Header>

        <Table celled>
          <Table.Header>
            <Table.Row textAlign="center">
              { type.attrNames.map((attrName, index) =>
                <Table.HeaderCell key={`${type.name}_header_${index}`}>
                  { (Array.isArray(type.headers) && type.headers[index]) || attrName }
                </Table.HeaderCell>
              )}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            { list.length > 0 ?
              list.map((obj, index) =>
                <Table.Row key={`${type.name}_${index}`}>
                  { type.attrNames.map((attrName) =>
                    <Table.Cell key={`${type.name}_${index}_${attrName}`}>
                      {obj[attrName]}
                    </Table.Cell>
                  )}
                </Table.Row>
              )
              :
              <Table.Row>
                <Table.Cell>데이터가 없습니다.</Table.Cell>
              </Table.Row>
            }
          </Table.Body>
        </Table>

        <Divider hidden />
      </Fragment>
    );
  }
}

export const type = ItemAttrListView.type;
export default ItemAttrListView;

