
import { extendObservable, computed } from 'mobx';


export class ItemModel {

  constructor(item = {}) {
    //
    const {
      status,
      location,
      sizeWeight,
      order,
      yardLocation,
      attrs,
      ...observableTarget
    } = item;

    // 원래의 프로퍼티명은 computed로 변환해서 쓰기 위해 observable*로 이름 변경
    extendObservable(this, {
      ...observableTarget,
      observableStatus: status,
      observableLocation: location,
      observableSizeWeight: sizeWeight,
      observableOrder: order,
      observableYardLocation: yardLocation,
      observableAttrs: attrs,
    });
  }


  @computed
  get status() {
    return this.getArrayProp('observableStatus');
  }

  @computed
  get location() {
    return this.getArrayProp('observableLocation');
  }

  @computed
  get sizeWeight() {
    return this.getArrayProp('observableSizeWeight');
  }

  @computed
  get order() {
    return this.getArrayProp('observableOrder');
  }

  @computed
  get yardLocation() {
    return this.getArrayProp('observableYardLocation');
  }

  @computed
  get attrs() {
    return this.getArrayProp('observableAttrs');
  }

  static fromApiModels(items) {
    //
    if (!Array.isArray(items)) {
      return [];
    }
    return items.map(item => ItemModel.fromApiModel(item));
  }

  static fromApiModel(item) {
    return new ItemModel(item);
  }

  /**
   * Ui Model을 Api Model로 변환
   */
  toApiModel() {
    return this;
  }

  /**
   * NameValue target
   */
  asNameValues() {
    return {
      nameValues: [
        { name: 'itemNo',       value: JSON.stringify(this.itemNo) },
        { name: 'parentNo',     value: JSON.stringify(this.parentNo) },
        { name: 'shapeType',    value: JSON.stringify(this.shapeType) },
        { name: 'stage',        value: JSON.stringify(this.stage) },
        { name: 'cutDateTime',  value: JSON.stringify(this.cutDateTime) },
      ],
    };
  }

  getArrayProp(name) {
    return this[name] ? this[name].slice() : [];
  }
}


export default ItemModel;
