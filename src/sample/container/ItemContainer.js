
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import { mesAutobind } from '@mes/mes-shared';
import { ContentLayout } from '@mes/mes-ui-react';
import ItemView from '../view/ItemView';


@inject('itemStore')
@mesAutobind
@observer
class ItemContainer extends Component {
  //
  componentDidMount() {
    //
    const { match } = this.props;
    const { params } = match;

    if (params.itemNo) {
      this.findItem(params.itemNo);
    }
  }

  componentWillUnmount() {
    this.props.itemStore.clear();
  }

  findItem(itemNo) {
    this.props.itemStore.findItem(itemNo);
  }

  linkToList() {
    this.props.history.push(`/items`);
  }


  render() {
    //
    const { itemStore } = this.props;
    const { item } = itemStore;

    return (
      <ContentLayout>
        <ItemView
          item={item}
        />
      </ContentLayout>
    );
  }
}

export default ItemContainer;
