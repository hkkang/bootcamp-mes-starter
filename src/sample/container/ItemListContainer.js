
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import { mesAutobind } from '@mes/mes-shared';
import { ContentLayout } from '@mes/mes-ui-react';

import ItemListView from '../view/ItemListView';


@inject('itemStore')
@mesAutobind
@observer
class ItemListContainer extends Component {
  //
  componentDidMount() {
    this.findAllItems();
  }

  componentWillUnmount() {
    this.props.itemStore.clear();
  }

  findAllItems() {
    this.props.itemStore.findAllItems();
  }

  render() {
    //
    const { itemStore } = this.props;

    return (
      <ContentLayout>
        <ItemListView
          items={itemStore.items}
        />
      </ContentLayout>
    );
  }
}

export default ItemListContainer;
