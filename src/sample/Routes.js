
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import ItemList from './container/ItemListContainer';
import Item from './container/ItemContainer';


const Routes = () => (
  <Route path="/items" render={({ match }) =>
    <Switch>
      <Route exact path={`${match.path}`} render={(props) => <ItemList {...props} /> } />
      <Route exact path={`${match.path}/:itemNo`} render={(props) => <Item {...props} />} />
    </Switch>
  }
  />
);

export default Routes;
