
import React, { Component } from 'react';

// import { Layout } from '@mes/mes-ui-react';


class StarterLayout extends Component {
  //
  render() {
    //
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default StarterLayout;
