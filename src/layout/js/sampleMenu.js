

const sampleMenu = [
  { name: '즐겨찾기', part: '냉연', children: [
    { name: '즐겨찾기 폴더 1', children: [
      { name: 'Items',        to: '/items' },
      { name: 'Item detail',  to: '/items/1' },
      { name: '광양자동차강판진행현황분석' },
      { name: '열연정정 소재현황 분석' },
      { name: '열연정정단위별모니터링' },
    ]},
    { name: '즐겨찾기 폴더 2', children: [
      { name: 'Status별진행현황분석' },
      { name: '광양재료주문진행현황분석' },
      { name: '광양자동차강판진행현황분석' },
      { name: '열연정정 소재현황 분석' },
      { name: '열연정정단위별모니터링' },
    ]},
  ]},
];


export default sampleMenu;
