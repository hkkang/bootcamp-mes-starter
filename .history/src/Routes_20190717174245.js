
import React, { Fragment } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';

import { StarterLayout } from './layout';

// Modules
// import { Routes as SampleRoutes } from './sample';
// import ItemList from './sample/container/ItemListContainer';


const contextPath = ''; // context.path();

const Routes = () => (
  <BrowserRouter basename={contextPath}>
    {/* <StarterLayout> */}
      <Switch>
        <Redirect exact from ="/" to="/items" />
        {/* <Route path="/items" render={(props) => <ItemList {...props} /> } /> */}
        <div>Hello</div>

        {/* <Route path="" render={() =>
          <Fragment>
            <SampleRoutes />
          </Fragment>
        }
        /> */}
      </Switch>
    {/* </StarterLayout> */}
  </BrowserRouter>
);

export default Routes;
