
import React, { Fragment } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';

import { StarterLayout } from './layout';

// Modules
import { Routes as SampleRoutes } from './sample';


const contextPath = ''; // context.path();

const Routes = () => (
  <BrowserRouter basename={contextPath}>
    <StarterLayout>
      <Switch>
        <Redirect exact from ="/" to="/items" />

        {/* <Route path="" render={() =>
          <Fragment>
            <SampleRoutes />
          </Fragment>
        }
        /> */}
      </Switch>
    </StarterLayout>
  </BrowserRouter>
);

export default Routes;
