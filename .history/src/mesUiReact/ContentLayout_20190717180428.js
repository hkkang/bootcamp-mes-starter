
import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';


class ContentLayout extends Component {
  //
  render() {
    return (
      <Container>
        {this.props.children}
      </Container>
    );
  }
}

export defualt ContentLayout;
