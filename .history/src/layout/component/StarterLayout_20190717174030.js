
import React, { Component } from 'react';

// import { Layout } from '@mes/mes-ui-react';
import { Container } from 'semantic-ui-react';


class StarterLayout extends Component {
  //
  render() {
    //
    return (
      <Container>
        {this.props.children}
      </Container>
    );
  }
}

export default StarterLayout;
