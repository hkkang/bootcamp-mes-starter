
import React, { Component } from 'react';

// import { Layout } from '@mes/mes-ui-react';
import { Container } from 'semantic-ui-react';
import sampleMenu from '../js/sampleMenu';


class StarterLayout extends Component {
  //
  render() {
    //
    return (
      <Container
        {...this.props}
      >
        {this.props.children}
      </Container>
    );
  }
}

export default StarterLayout;
