
import React, { Component } from 'react';

import { Layout } from '@mes/mes-ui-react';
import sampleMenu from '../js/sampleMenu';


class StarterLayout extends Component {
  //
  render() {
    //
    return (
      <Layout
        {...this.props}
        // type="Mobile"
        menuItems={sampleMenu}
        /**
         * 아래 두가지 중, 상황에 맞는 props를 사용하여 권한을 가져와서 개발
         * 1. usingLoginPage : login 화면을 사용하여, 직번 셋팅 및 권한 목록 가져올 때 사용
         *                     login 후, main에 입력한 경로로 페이지를 이동시키기 떄문에,
         *                     Routes.js에 <Route exact path="/items" .. /> 와 같이 exact를 사용하여 main 경로에 해당하는 component 정의 필수
         * 2. account : login 화면을 사용하지 않고, 직번 셋팅 및 권한 목록을 가져올 때 사용
         * security 서비스를 디버깅 시마다 과도하게 호출하는 문제가 발생하여, 권한 목록이 필요한 경우에만 추가해야 함
         */

        /** login 화면을 사용하지 않고, 직번을 통해 권한 목록을 가져올 떄 사용할 props (ex. PD000000)  */
        //account="PDxxxxxx"

        /** login 화면을 사용할 때 usingLoginPage를 true로 설정 */
        //usingLoginPage
        /** login 화면을 사용할 때 로그인 후 이동할 main 페이지 url 설정 */
        //main="/items"
      >
        {this.props.children}
      </Layout>
    );
  }
}

export default StarterLayout;
