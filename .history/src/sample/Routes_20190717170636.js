
import React from 'react';
import { Route } from 'react-router-dom';
import { RouteGroup } from '@mes/mes-ui-react';

import ItemList from './container/ItemListContainer';


const Routes = () => (
  <RouteGroup path="/items">
    <Route exact render={(props) => <ItemList {...props} />} />
  </RouteGroup>
);

export default Routes;
