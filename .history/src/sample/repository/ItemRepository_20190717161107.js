
import axios from 'axios';
import { instance } from '@mes/mes-shared';
import ItemModel from '../model/ItemModel';


class ItemRepository {

  @instance
  static instance;


  findAllItems() {
    return axios.get('/mes-starter-service/items')
      .then(response => ItemModel.fromApiModels(response.data));
  }

  findItem(itemNo) {
    return axios.get(`/mes-starter-service/items/${itemNo}`)
      .then(response => ItemModel.fromApiModel(response.data));
  }
}

export default ItemRepository;
