
import { observable, computed, action } from 'mobx';
import { mesAutobind, instance } from '@mes/mes-shared';
import ItemRepository from '../repository/ItemRepository';


@mesAutobind
class ItemStore {

  @instance(ItemRepository.instance)
  static instance;

  repository;

  @observable
  _items = [];

  @observable
  item = null;


  constructor(repository) {
    this.repository = repository;
  }

  @computed
  get items() {
    return this._items ? this._items.slice() : [];
  }


  @action
  clear() {
    this._items = null;
    this._item = null;
  }

  @action
  findAllItems() {
    return ItemRepository.instance.findAllItems()
      .then(action(items => this._items = items));
  }

  @action
  findItem(itemNo) {
    return ItemRepository.instance.findItem(itemNo)
      .then(action(item => this.item = item));
  }

  @action
  setItemProp(name, value) {
    this.item[name] = value;
  }
}

export default ItemStore;

