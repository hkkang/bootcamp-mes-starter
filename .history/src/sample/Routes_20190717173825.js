
import React from 'react';
import { Route } from 'react-router-dom';
import { RouteGroup } from '@mes/mes-ui-react';

import ItemList from './container/ItemListContainer';


const Routes = () => (
  <Route path="/items" render={({ match }) =>
    <Route render={(props) => <ItemList {...props} /> } />
  }
  />
);

export default Routes;
