
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

// import { mesAutobind, context } from '@/mesShared';
import dd from '@/mesShared';
// import { mesAutobind, context } from '@mes/mes-shared';
// import { ContentLayout } from '@mes/mes-ui-react';

import ItemListView from '../view/ItemListView';
import SnapChartView from '../view/SnapChartView';

console.log('dd', dd);

@inject('itemStore', 'chartStore')
// @mesAutobind
@observer
class ItemListContainer extends Component {
  //
  componentDidMount() {
    this.findAllItems();
  }

  componentWillUnmount() {
    this.props.itemStore.clear();
    this.props.chartStore.clear();
  }

  findAllItems() {
    this.props.itemStore.findAllItems();
  }

  render() {
    //
    const {
      itemStore, chartStore,
    } = this.props;

    return (
      <div>
        Hello
      </div>
    );
  }
}

export default ItemListContainer;
