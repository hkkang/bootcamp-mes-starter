
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import { mesAutobind } from '@/mesShared';
import { ContentLayout } from '@/mesUiReact';


@inject('itemStore')
@mesAutobind
@observer
class ItemListContainer extends Component {
  //
  componentDidMount() {
    this.findAllItems();
  }

  componentWillUnmount() {
    this.props.itemStore.clear();
    this.props.chartStore.clear();
  }

  findAllItems() {
    this.props.itemStore.findAllItems();
  }

  render() {
    //
    return (
      <ContentLayout>
        Hello Bootcamp
      </ContentLayout>
    );
  }
}

export default ItemListContainer;
