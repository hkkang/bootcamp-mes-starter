
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

// import { mesAutobind, context } from '@/mesShared';
import dd from '@/mesShared';
// import { mesAutobind, context } from '@mes/mes-shared';
// import { ContentLayout } from '@mes/mes-ui-react';
// import { ChartExampleSnap } from '@mes/mes-snap-example';

import ItemListView from '../view/ItemListView';
import SnapChartView from '../view/SnapChartView';

console.log('dd', dd);

@inject('itemStore', 'chartStore')
// @mesAutobind
@observer
class ItemListContainer extends Component {
  //
  componentDidMount() {
    this.findAllItems();
    this.props.chartStore.setDataType(ChartExampleSnap.DATA_TYPE.first);
  }

  componentWillUnmount() {
    this.props.itemStore.clear();
    this.props.chartStore.clear();
  }

  findAllItems() {
    this.props.itemStore.findAllItems();
  }

  handleToggleChartDataType() {
    //
    const { chartStore } = this.props;
    const { DATA_TYPE } = ChartExampleSnap;
    const nextDataType = chartStore.dataType === DATA_TYPE.first ? DATA_TYPE.second : DATA_TYPE.first;

    chartStore.setDataType(nextDataType);
  }

  handleClickItemWindow(e, { itemNo }) {
    window.open(`${context.path()}/items/${itemNo}?folding=true`, null, 'toolbars=no, location=no,')
  }

  render() {
    //
    const {
      itemStore, chartStore,
    } = this.props;

    return (
      <ContentLayout>
        <ContentLayout.Header
          title="Item List"
        />

        <ItemListView
          items={itemStore.items}
          onClickItemWindow={this.handleClickItemWindow}
        />

        <SnapChartView
          onChangeDataType={this.handleToggleChartDataType}
        >
          <ChartExampleSnap
            dataType={chartStore.dataType}
          />
        </SnapChartView>
      </ContentLayout>
    );
  }
}

export default ItemListContainer;
