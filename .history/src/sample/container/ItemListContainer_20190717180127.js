
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

// import { mesAutobind, context } from '@/mesShared';
import { mesAutobind } from '@/mesShared';
// import { mesAutobind, context } from '@mes/mes-shared';
// import { ContentLayout } from '@mes/mes-ui-react';


@inject('itemStore')
@mesAutobind
@observer
class ItemListContainer extends Component {
  //
  componentDidMount() {
    this.findAllItems();
  }

  componentWillUnmount() {
    this.props.itemStore.clear();
    this.props.chartStore.clear();
  }

  findAllItems() {
    this.props.itemStore.findAllItems();
  }

  render() {
    //
    return (
      <div>
        Hello Bootcamp
      </div>
    );
  }
}

export default ItemListContainer;
