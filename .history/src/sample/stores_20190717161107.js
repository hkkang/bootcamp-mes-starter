
import ItemStore from './store/ItemStore';
import ChartStore from './store/ChartStore';
import ItemUiStore from './store/ItemUiStore';


export default {
  itemStore: ItemStore.instance,
  chartStore: ChartStore.instance,
  itemUiStore: ItemUiStore.instance,
};
