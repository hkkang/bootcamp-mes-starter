
import React from 'react';
import { Route } from 'react-router-dom';

import ItemList from './container/ItemListContainer';


const Routes = () => (
  <Route path="/items" render={({ match }) =>
    <Route render={(props) => <ItemList {...props} /> } />
  }
  />
);

export default Routes;
