
import React from 'react';
import { BrowserRouter, Switch, Redirect } from 'react-router-dom';

import { RouteGroup } from '@mes/mes-ui-react';
import { StarterLayout } from './layout';

// Modules
import { Routes as SampleRoutes } from './sample';


const contextPath = ''; // context.path();

const Routes = () => (
  <BrowserRouter basename={contextPath}>
    <StarterLayout>
      <Switch>
        <Redirect exact from ="/" to="/items" />

        <Route path="" render={() =>
          <Fragment>
            <SampleRoutes />
          </Fragment>
        }
        />
      </Switch>
    </StarterLayout>
  </BrowserRouter>
);

export default Routes;
