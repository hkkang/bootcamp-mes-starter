
// Style --------------------------------------------------
import '@mes/mes-ui-react/dist/lib/component.css';
// import '@mes/mes-ui-react/dist/lib/mobile-posco.css';
import 'semantic-ui-css/semantic.css';

// JS -----------------------------------------------------
import { configure } from 'mobx';
// import { context } from '@mes/mes-shared';


configure({
  enforceActions: 'observed',
});
// context.init(process.env);
// console.log(`[mes-starter] Your env is ${JSON.stringify(process.env, null, 2)}`);
