
const path = require('path');
const fs = require('fs');
const {
  paths,
} = require('react-app-rewired');

const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin,
} = require('customize-cra');
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);


module.exports = {
  webpack: (config, env) => {
    return override(
      addDecoratorsLegacy(),
      removeModuleScopePlugin(),
      addWebpackAlias({
        '@': `${paths.appPath}/src`,
      }),
    )(config, env);
  },
  paths: (paths, env) => {
    console.log('start', paths.appHtml);
    paths.appHtml = resolveApp('src/index.html');
    console.log('end', paths.appHtml);
    console.dir(paths);
    return null;
  },
};
