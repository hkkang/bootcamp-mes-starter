
const path = require('path');
const fs = require('fs');
const {
  paths,
} = require('react-app-rewired');

const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin,
} = require('customize-cra');


const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);

module.exports = {
  webpack: (config, env) => {
    return override(
      addDecoratorsLegacy(),
      removeModuleScopePlugin(),
      addWebpackAlias({
        '@': `${paths.appPath}/src`,
      }),
    )(config, env);
  },
  paths: (paths, env) => {
    paths.appHtml = resolveApp('src/index.html');
    return paths;
  },
};
