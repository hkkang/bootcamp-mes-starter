
const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin
} = require('customize-cra');


module.exports = {
  webpack: override(
    addDecoratorsLegacy(),
    addWebpackAlias({
      '@': '',
    }),
  ),
};
