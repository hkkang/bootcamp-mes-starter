
const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
} = require('customize-cra');


module.exports = {
  webpack: override(
    addDecoratorsLegacy(),
    addWebpackAlias({
      '@': 'src'
    }),
  ),
};
