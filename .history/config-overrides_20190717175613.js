
const path = require('path');
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);
const {
  paths,
} = require('react-app-rewired');

const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin,
} = require('customize-cra');

module.exports = {
  webpack: (config, env) => {
    console.log('paths', paths);
    return override(
      addDecoratorsLegacy(),
      removeModuleScopePlugin(),
      addWebpackAlias({
        '@': `${paths.appPath}/src`,
      }),
    )(config, env);
  },
  path: (paths, env) => {
    console.dir(paths);
    paths.appHtml = resolveApp('src/index.html');
    return null;
  },
};
