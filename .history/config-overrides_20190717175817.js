
const path = require('path');
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);
const {
  paths,
} = require('react-app-rewired');

const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin,
} = require('customize-cra');

module.exports = {
  webpack: (config, env) => {
    return;
    return override(
      addDecoratorsLegacy(),
      removeModuleScopePlugin(),
      addWebpackAlias({
        '@': `${paths.appPath}/src`,
      }),
    )(config, env);
  },
  path: (paths, env) => {
    console.log('start', paths.appHtml);
    paths.appHtml = resolveApp('src/index.html');
    console.log('end', paths.appHtml);
    console.dir(paths);
    return paths;
  },
};
