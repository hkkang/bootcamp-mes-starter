
const {
  paths,
} = require('react-app-rewired');

const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin,
} = require('customize-cra');


module.exports = {
  webpack: (config, env) => {
    console.dir(env);
    console.log('paths', paths);
    return null;
    return override(
      addDecoratorsLegacy(),
      removeModuleScopePlugin(),
      addWebpackAlias({
        '@': 'src',
      }),
    )(config, env);
  },
};
