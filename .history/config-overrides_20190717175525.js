
const {
  paths,
} = require('react-app-rewired');

const {
  override,
  addDecoratorsLegacy,
  addWebpackAlias,
  removeModuleScopePlugin,
} = require('customize-cra');

const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
  webpack: (config, env) => {
    console.log('paths', paths);return null;
    return override(
      addDecoratorsLegacy(),
      removeModuleScopePlugin(),
      addWebpackAlias({
        '@': `${paths.appPath}/src`,
      }),
    )(config, env);
  },
  path: (paths, env) => {
    console.dir(paths);
    return null;
  },
};
